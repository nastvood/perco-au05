#ifndef GVERSION_H
#define GVERSION_H

#define VERSION 0,0,3,0
#define VERSION_STR "0.0.3.0"
#define PROG_NAME "PERCo-AU05"
#define ORG_NAME "Gamma, Orel"

#endif // GVERSION_H
