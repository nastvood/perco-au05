#include "mainwindow.h"
#include "ui_mainwindow.h"

#define VALUE_ROLE Qt::UserRole + 1
#define MESS_ROW_MAX 100

#define WAIT_FOR_BYTES_WRITTEN 1000
#define WAIT_FOR_READY_READ 5000
#define TEST_TIME 3000

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    setWindowTitle(PROG_NAME);

    actAbout = new QAction("О программе",this);
    connect(actAbout,SIGNAL(triggered()),SLOT(showAbout()));
    ui->menuBar->addAction(actAbout);

    initToolBar();
    initTray();

    ui->twMess->setColumnCount(3);
    ui->twMess->setColumnWidth(0,30);

    settings = new QSettings(ORG_NAME,PROG_NAME,this);

    ui->cmbParity->insertItem(0,"Без контроля");
    ui->cmbParity->insertItem(1,"Четный");
    ui->cmbParity->insertItem(2,"Не четный");

    ui->cmbCom->clear();
    QList<QSerialPortInfo> ports = QSerialPortInfo::availablePorts();
    for (int i = 0; i< ports.size(); i++){
        const QSerialPortInfo info = ports.at(i);
        ui->cmbCom->insertItem(i,QString("%1(%2)").arg(info.portName()).arg(info.description()));
        ui->cmbCom->setItemData(i,info.portName(),VALUE_ROLE);
    }

    initSettings();

    connect(&timer,SIGNAL(timeout()),SLOT(sendTime()));
    connect(&timerTest,SIGNAL(timeout()),SLOT(sendTimeTest()));

    if (autoStart)
        start();
}

MainWindow::~MainWindow()
{
    delete ui;

    if (port.isOpen())
        port.close();
}

void MainWindow::stop()
{
    timer.stop();

    if (port.isOpen())
        port.close();
    else
        return;

    showMessage("Запись в табло остановлена",Qt::blue);
}

void MainWindow::search()
{
    stop();

    QTime time = QDateTime::currentDateTime().time();
    time = time.addSecs(addToTime);

    unsigned char mess[8]={0};
    prepareMessage(mess,time.second(),time.minute(),time.hour());

    for(int i = 0;i < ui->cmbCom->count();i++){
        QString portName = ui->cmbCom->itemData(i,VALUE_ROLE).toString();
        QSerialPort checkedPort;

        adjustPort(checkedPort,portName);

        showMessage(QString("Проверяю порт %1").arg(checkedPort.portName()),Qt::blue);

        if (!checkedPort.open(QIODevice::ReadWrite)){
            showMessage("Ошибка открытия порта "+checkedPort.errorString(),Qt::red);
            continue;
        }

        int ret = checkedPort.write((const char *)mess,sizeof(mess));
        checkedPort.waitForBytesWritten(WAIT_FOR_BYTES_WRITTEN);
        if (ret == sizeof(mess)){
            checkedPort.waitForReadyRead(WAIT_FOR_READY_READ);
            QByteArray data = checkedPort.readAll();
            if (data.size() == 2){
                if(data.toHex() == "fced"){
                    showMessage(QString("Табло найдено на %1").arg(checkedPort.portName()),Qt::green);
                    setCmbPortsByPortName(checkedPort.portName());
                    continue;
                }
            }
        }

        showMessage(QString("На %1 табло не найдено").arg(checkedPort.portName()),Qt::blue);

        checkedPort.close();
    };
}

void MainWindow::test()
{
    stop();

    if (!openPort())
        return;

    testVals<<qMakePair(0,0)<<qMakePair(11,11)<<qMakePair(22,22)<<qMakePair(23,33)
              <<qMakePair(14,44)<<qMakePair(15,55);

    timerTest.start(TEST_TIME);
}

void MainWindow::start()
{
    if (!timer.isActive())
        timer.start(period * 1000);
}

void MainWindow::sendTime()
{
    if (!openPort())
        return;

    QTime time = QDateTime::currentDateTime().time();
    time = time.addSecs(addToTime);

    unsigned char mess[8]={0};

    int ret = port.write((const char *)prepareMessage(mess,time.second(),time.minute(),time.hour()),sizeof(mess));
    if (ret == sizeof(mess)){
        QByteArray data = port.readAll();
        if (data.size() == 2){
            if(data.toHex() == "fced"){
                showMessage(QString("В табло записано время %1")
                            .arg(time.toString("HH:mm:ss")),
                        Qt::green);
            }
        }
    }

    showMessage("Ошибка записи в табло",Qt::red);
}

void MainWindow::sendTimeTest()
{
    if (testVals.isEmpty()){
        timerTest.stop();
        return;
    }

    unsigned char mess[8]={0};

    uchar hour = testVals.head().first;
    uchar minute = testVals.dequeue().second;

    showMessage(QString("Тест табло записываю значение %1")
                .arg(hour * 100 + minute),
            Qt::green);

    port.write((const char *)prepareMessage(mess,0,minute,hour),sizeof(mess));
}

uchar *MainWindow::prepareMessage(uchar *mess,uchar second,uchar minute,uchar hour)
{
    mess[0] = 0xAA;
    mess[1] = 0x04;
    mess[2] = 0x38;

    mess[3] = second;
    mess[4] = minute;
    mess[5] = hour;

    WORD_VAL crc;
    crc.Val = 0;
    GetCRC((unsigned char *)&mess[1],&crc,5);

    mess[6] = (255 - crc.v[0]);
    mess[7] = (255 - crc.v[1]);

    return mess;
}

void MainWindow::adjustPort(QSerialPort &serialPort,const QString &portName)
{
    serialPort.setPortName(portName);
    serialPort.setDataBits((QSerialPort::DataBits)dataBits);
    serialPort.setBaudRate(baudRate);
    serialPort.setStopBits((QSerialPort::StopBits)stopBit);
    serialPort.setParity((QSerialPort::Parity)parity);
}

void MainWindow::showAbout()
{
    QString about = QString("<font size=12><b>%1</b></font><br><br>Основан на Qt %2<br><br>Версия: %3<br><br>Собран %4 %5<br><br>&copy;ОАО Гамма, %6")
            .arg(PROG_NAME)
            .arg(QT_VERSION_STR)
            .arg(VERSION_STR)
            .arg(QString::fromLocal8Bit(__DATE__))
            .arg(QString::fromLocal8Bit(__TIME__))
            .arg(QDate::currentDate().year());
    QMessageBox::about(this,PROG_NAME,about);
}

void MainWindow::trayActivated(QSystemTrayIcon::ActivationReason reason)
{
    switch (reason) {
        case QSystemTrayIcon::Trigger:
            this->show();
            break;
        case QSystemTrayIcon::DoubleClick:
            this->show();
            break;
        default:
            break;
    }
}

void MainWindow::closeEvent(QCloseEvent *e)
{
    if (tray->isVisible()){
        hide();
        e->ignore();
    }
}

void MainWindow::initTray()
{
    tray = new QSystemTrayIcon(QIcon(":/img/time.png"),this);
    tray->show();
    connect(tray,SIGNAL(activated(QSystemTrayIcon::ActivationReason)),SLOT(trayActivated(QSystemTrayIcon::ActivationReason)));

    tray->setToolTip(PROG_NAME);

    trayMenu = new QMenu(this);

    trayMenu->addAction(actSearch);
    trayMenu->addAction(actTest);
    trayMenu->addSeparator();
    trayMenu->addAction(actStart);
    trayMenu->addAction(actStop);
    trayMenu->addSeparator();

    actExit = new QAction(QIcon(":/img/exit.png"),"Выход",this);
    connect(actExit,SIGNAL(triggered()),qApp,SLOT(quit()));
    trayMenu->addAction(actExit);

    tray->setContextMenu(trayMenu);
}

void MainWindow::initToolBar()
{
    actSearch = new QAction(QIcon(":/img/search.png"),"Поиск табло",this);
    connect(actSearch,SIGNAL(triggered()),SLOT(search()));
    ui->toolBar->addAction(actSearch);

    actTest = new QAction(QIcon(":/img/test.png"),"Тест табло",this);
    connect(actTest,SIGNAL(triggered()),SLOT(test()));
    ui->toolBar->addAction(actTest);

    ui->toolBar->addSeparator();

    actStart = new QAction(QIcon(":/img/start.png"),"Пуск",this);
    connect(actStart,SIGNAL(triggered()),SLOT(start()));
    ui->toolBar->addAction(actStart);

    actStop = new QAction(QIcon(":/img/stop.png"),"Стоп",this);
    connect(actStop,SIGNAL(triggered()),SLOT(stop()));
    ui->toolBar->addAction(actStop);

    ui->toolBar->addSeparator();

    actClose = new QAction(QIcon(":/img/exit.png"),"Закрыть",this);
    connect(actClose,SIGNAL(triggered()),SLOT(close()));
    ui->toolBar->addAction(actClose);
}

void MainWindow::setCmbPortsByPortName(const QString &portName)
{
    for (int i = 0;i < ui->cmbCom->count(); i++){
        QString cmbValue = ui->cmbCom->itemData(i,VALUE_ROLE).toString();
        if (cmbValue == portName)
            ui->cmbCom->setCurrentIndex(i);
    }
}

bool MainWindow::openPort()
{
    QString portName = ui->cmbCom->currentData(VALUE_ROLE).toString();

    if (port.portName() != portName){
        if (port.isOpen()){
            showMessage("Порт "+port.portName()+ " - открыт, закрываю",Qt::blue);
            port.close();
        }
    }else{
        if(port.isOpen())
            return true;
    }

    showMessage("Пытаюсь открыть порт "+portName,Qt::blue);

    adjustPort(port,portName);

    if (port.open(QIODevice::ReadWrite)){
        showMessage("Порт " + portName + " - открыт",Qt::green);
        return true;
    }else{
        showMessage("Ошибка открытия порта "+port.errorString(),Qt::red);
    }

    return false;
}

void MainWindow::showMessage(const QString &message,Qt::GlobalColor color)
{
    if (ui->twMess->rowCount() >= MESS_ROW_MAX){
        ui->twMess->removeRow(MESS_ROW_MAX - 1);
    }

    ui->twMess->insertRow(0);

    QTableWidgetItem *item = new QTableWidgetItem();
    item->setBackground(QBrush(color));
    ui->twMess->setItem(0,0,item);

    ui->twMess->setItem(0,1,new QTableWidgetItem(QDateTime::currentDateTime().toString("hh:mm:ss.zzz")));
    ui->twMess->setItem(0,2,new QTableWidgetItem(message));
}

void MainWindow::initSettings()
{
    QString settingPort = settings->value("port","COM1").toString();
    setCmbPortsByPortName(settingPort);

    baudRate = settings->value("baudRate",19200).toInt();
    ui->cmbBaudRate->setCurrentText(QString().number(baudRate));

    dataBits = settings->value("dataBits",8).toInt();
    ui->sbDataBits->setValue(dataBits);

    stopBit= settings->value("stopBit",1).toInt();
    ui->sbStopBit->setValue(stopBit);

    parity = (QSerialPort::Parity)settings->value("Parity",0).toInt();
    ui->cmbParity->setCurrentIndex(parity);

    addToTime = settings->value("addToTime",0).toInt();
    ui->sbAddToTime->setValue(addToTime);

    period = settings->value("period",1).toUInt();
    ui->sbPeriod->setValue(period);

    autoStart = settings->value("autoStart",0).toBool();
    ui->cbAutoStart->setChecked(autoStart);

    QSettings runSettings("HKEY_CURRENT_USER\\Software\\Microsoft\\Windows\\CurrentVersion\\Run",QSettings::NativeFormat);
    if (runSettings.allKeys().indexOf(PROG_NAME) > -1)
        ui->cbAutoStartWin->setChecked(true);
    else
        ui->cbAutoStartWin->setChecked(false);
}

void MainWindow::on_pbSaveSattings_clicked()
{
    QString portName = ui->cmbCom->itemData(ui->cmbCom->currentIndex(),VALUE_ROLE).toString();
    settings->setValue("port",portName);

    baudRate = ui->cmbBaudRate->currentText().toInt();
    settings->setValue("baudRate",baudRate);

    dataBits = ui->sbDataBits->value();
    settings->setValue("dataBits",dataBits);

    stopBit = ui->sbStopBit->value();
    settings->setValue("stopBit",stopBit);

    parity = (QSerialPort::Parity)ui->cmbParity->currentIndex();
    settings->setValue("Parity",parity);

    addToTime = ui->sbAddToTime->value();
    settings->setValue("addToTime",addToTime);

    period = ui->sbPeriod->value();
    settings->setValue("period",period);

    autoStart = ui->cbAutoStart->isChecked();
    settings->setValue("autoStart",autoStart);

    QSettings runSettings("HKEY_CURRENT_USER\\Software\\Microsoft\\Windows\\CurrentVersion\\Run",QSettings::NativeFormat);
    if (ui->cbAutoStartWin->isChecked()){
        QString path = QDir::toNativeSeparators(qApp->applicationFilePath()) + " /min";
        runSettings.setValue(PROG_NAME,path);
    }else{
        runSettings.remove(PROG_NAME);
    }

    QMessageBox::information(this,PROG_NAME,"Настройки сохранены");
}
