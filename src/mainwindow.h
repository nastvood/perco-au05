#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSettings>
#include <QDateTime>
#include <QTimer>
#include <QSystemTrayIcon>
#include <QCloseEvent>
#include <QMessageBox>
#include <QDir>
#include <QThread>
#include <QQueue>
#include <QPair>
#include <QMessageBox>
#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>

#include "crc.h"
#include "gversion.h"

#include <QDebug>

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

    public:
        explicit MainWindow(QWidget *parent = 0);
        ~MainWindow();

    private slots:
        void start();
        void stop();
        void search();
        void test();
        void on_pbSaveSattings_clicked();
        void showMessage(const QString &message, Qt::GlobalColor color = Qt::green);
        void sendTime();
        void sendTimeTest();
        void showAbout();
        void trayActivated(QSystemTrayIcon::ActivationReason reason);

    private:
        Ui::MainWindow *ui;
        QSerialPort port;
        QSettings *settings;
        QTimer timer;
        QTimer timerTest;
        QQueue<QPair<unsigned char,unsigned char> > testVals;

        QAction *actSearch;
        QAction *actTest;
        QAction *actStart;
        QAction *actStop;
        QAction *actExit;
        QAction *actClose;
        QAction *actAbout;
        QMenu *mnAbout;

        QSystemTrayIcon *tray;
        QMenu *trayMenu;

        int baudRate;
        int dataBits;
        int stopBit;
        QSerialPort::Parity parity;
        unsigned int period;
        int addToTime;
        bool autoStart;

        void initTray();
        void closeEvent(QCloseEvent *e);
        void initToolBar();
        void setCmbPortsByPortName(const QString &portName);
        void initSettings();
        bool openPort();
        uchar *prepareMessage(uchar *mess, uchar second, uchar minute, uchar hour);
        void adjustPort(QSerialPort &serialPort, const QString &portName);
};

#endif // MAINWINDOW_H
