#-------------------------------------------------
#
# Project created by QtCreator 2014-07-15T16:31:54
#
#-------------------------------------------------

QT       += core gui serialport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = PERCo-AU05
TEMPLATE = app

QMAKE_LFLAGS = -static -static-libgcc

SOURCES += main.cpp\
        mainwindow.cpp \
    crc.cpp

HEADERS  += mainwindow.h \
    crc.h \
    gversion.h

FORMS    += mainwindow.ui

RESOURCES += \
    res.qrc

OTHER_FILES += \
    win.rc

RC_FILE = win.rc
