#include "crc.h"

void GetCRC(unsigned char *p, WORD_VAL *crc, unsigned short cntr)
{
    unsigned char index;

    while(cntr--){
        index = crc->v[0] ^ *p++;
        crc->v[0] = Tablo[index] ^ crc->v[1];
        crc->v[1] = Tabhi[index];
    }

    return;
};

